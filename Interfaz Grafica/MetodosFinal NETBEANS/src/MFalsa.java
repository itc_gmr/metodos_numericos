import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.math.plot.Plot2DPanel;
import org.nfunk.jep.JEP;
public class MFalsa extends javax.swing.JInternalFrame {
    Plot2DPanel grafica = new Plot2DPanel();
    JEP funcion = new JEP();
    MGrafica Graficacion;
    private DefaultTableModel Model;
    int con=0;
    public MFalsa() {
        initComponents();
        setSize(960,500); 
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtfuncion = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txta = new javax.swing.JTextField();
        txtb = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txterror = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtni = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtraiz = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setTitle("Metodo de Falsa Posicion");
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel1.setText("Funcion");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(10, 130, 60, 24);

        txtfuncion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfuncionActionPerformed(evt);
            }
        });
        jPanel1.add(txtfuncion);
        txtfuncion.setBounds(60, 130, 170, 30);

        jButton1.setText("Calcular la Raiz");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(140, 400, 150, 30);

        jLabel2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel2.setText("Intervalo: a");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(10, 180, 90, 15);

        jLabel3.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel3.setText("Intervalo: b");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(10, 220, 80, 15);

        txta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtaKeyTyped(evt);
            }
        });
        jPanel1.add(txta);
        txta.setBounds(80, 170, 120, 30);

        txtb.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtbKeyTyped(evt);
            }
        });
        jPanel1.add(txtb);
        txtb.setBounds(80, 210, 120, 30);

        jLabel4.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel4.setText("Error Permitido");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 250, 130, 15);

        txterror.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txterrorActionPerformed(evt);
            }
        });
        txterror.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txterrorKeyTyped(evt);
            }
        });
        jPanel1.add(txterror);
        txterror.setBounds(110, 250, 110, 30);

        jLabel5.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel5.setText("Numero de Iteraciones Max");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 290, 180, 30);

        txtni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtniKeyTyped(evt);
            }
        });
        jPanel1.add(txtni);
        txtni.setBounds(180, 290, 100, 30);

        jLabel6.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel6.setText("Raiz");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(10, 340, 40, 20);

        txtraiz.setEnabled(false);
        jPanel1.add(txtraiz);
        txtraiz.setBounds(50, 340, 220, 30);

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/log.png"))); // NOI18N
        jPanel1.add(jLabel7);
        jLabel7.setBounds(10, 0, 240, 120);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/limpiar.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(10, 400, 40, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(10, 10, 300, 440);

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabla.setEnabled(false);
        jScrollPane2.setViewportView(tabla);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(320, 70, 610, 390);

        jLabel8.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel8.setText("Tabla de Procedimiento y Solucion");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(400, 20, 320, 50);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tecmini.png"))); // NOI18N
        getContentPane().add(jLabel9);
        jLabel9.setBounds(760, 0, 180, 70);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String data[][]={};
        String col[]={"i","Xi","f(Xi)","Xi-X1","f(Xi)-f(X1)","Xi+1","Error","f(Xi+1)"};
        Model=new DefaultTableModel(data,col);
        tabla.setModel(Model);
        con = 0;
        try {
            if(txtfuncion.getText().length() == 0 || txta.getText().length() == 0 || txtb.getText().length() == 0 || txterror.getText().length() == 0 || txtni.getText().length() == 0){
                JOptionPane.showMessageDialog(null, "ALguna de las cajas esta vacia");
                txtfuncion.requestFocus();
                return;
            }
            JEP j = new JEP();
            j.addStandardConstants();
            j.addStandardFunctions();
            String fun = txtfuncion.getText();
            Funcion CFun =new Funcion(fun);
            j.parseExpression(txta.getText());// pasamos a convertr la expresion ingresada en la casilla x0
            double a = j.getValue();
            j.parseExpression(txtb.getText());// pasamos a convertr la expresion ingresada en la casilla x1
            double b = j.getValue();
            j.parseExpression(txterror.getText());// pasamos a convertr la expresion ingresada en la casilla  de tolerancia
            double e = j.getValue();
            int ni = Integer.parseInt(txtni.getText());// pasamos a convertr la expresion ingresada en la casilla  de numero maximo de iteraciones
            double x1=a, xi=b, xim1=0; 
            int i=1;
            con = 0;
            System.out.println("Valor de Contador: " + con);
            Model.insertRow(con, new Object[]{});
            Model.setValueAt(i, con, 0);
            Model.setValueAt(xi, con, 1);
            Model.setValueAt(f(fun,xi), con, 2);
            Model.setValueAt((xi-x1), con, 3);
            int cont=0;
            double eps;
            do{ 
                i++;
                con++;  
		xim1=xi-((f(fun,xi)*(xi-x1))/(f(fun,xi)-f(fun,x1)));
		eps= Math.abs(xim1-xi);
		Model.insertRow(con, new Object[]{});
                Model.setValueAt(i, con, 0);
                Model.setValueAt(xi, con, 1);
                Model.setValueAt(f(fun,xi), con, 2);
                Model.setValueAt((xi-x1), con, 3);
                Model.setValueAt(f(fun,xi)-f(fun,x1), con, 4);
                Model.setValueAt(xim1, con, 5);
                Model.setValueAt(eps, con, 6);
                Model.setValueAt(f(fun,xim1), con, 7);
                
                xi = xim1;
        }while(i<=ni && eps>e);
        Graficacion = new MGrafica("Grafica Falsa Posición Función: " + txtfuncion.getText().toString());
        txtraiz.setText(Double.toString(xim1));
        Graficacion.AgregaDatos(CFun,xi);
        Graficacion.Agrega(xim1,ni);
        Graficacion.MuestraGrafica("Grafica Falsa Posición Función: " + txtfuncion.getText().toString());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR EN LA LECTURA DE DATOS", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    public double f(double x){
        funcion.addVariable("x",x);
        return funcion.getValue();
    }
    private double f(String funcion, double x){
        JEP f = new JEP();
        f.addStandardConstants();
        f.addStandardFunctions();
        f.addVariable("x", x);
        f.parseExpression(funcion); 
        return f.getValue();
    }
        public void limpiar(){
        txtfuncion.setText("");
        txta.setText("");
        txtb.setText("");
        txterror.setText("");
        txtni.setText("");
        txtraiz.setText("");
        Model=new DefaultTableModel();
        tabla.setModel(Model);   
    }
    private void txtfuncionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfuncionActionPerformed
    }//GEN-LAST:event_txtfuncionActionPerformed
    private void txterrorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txterrorActionPerformed
    }//GEN-LAST:event_txterrorActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
       limpiar();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txtaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtaKeyTyped
       char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txtaKeyTyped

    private void txtbKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbKeyTyped
        char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txtbKeyTyped

    private void txterrorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txterrorKeyTyped
        char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txterrorKeyTyped

    private void txtniKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtniKeyTyped
       char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txtniKeyTyped

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txta;
    private javax.swing.JTextField txtb;
    private javax.swing.JTextField txterror;
    private javax.swing.JTextField txtfuncion;
    private javax.swing.JTextField txtni;
    private javax.swing.JTextField txtraiz;
    // End of variables declaration//GEN-END:variables
}
