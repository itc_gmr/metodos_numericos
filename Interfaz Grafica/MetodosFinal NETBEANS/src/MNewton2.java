import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.math.plot.Plot2DPanel;
import org.nfunk.jep.JEP;
public class MNewton2 extends javax.swing.JInternalFrame {
    Plot2DPanel grafica = new Plot2DPanel();
    JEP funcion = new JEP();
    JEP Derivada = new JEP();
    JEP Derivada2 = new JEP();   
    MGrafica Graficacion;
    private DefaultTableModel Model;
    int con=0;
    public MNewton2() {
        initComponents();
        setSize(960,500);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtDerivada = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        txtfuncion = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtx = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txterror = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtni = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtraiz = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        txtDerivada.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDerivadaActionPerformed(evt);
            }
        });

        setTitle("Newton de 2do Orden");
        getContentPane().setLayout(null);

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabla.setEnabled(false);
        tabla.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tablaKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(tabla);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(320, 70, 610, 390);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel1.setLayout(null);

        txtfuncion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfuncionActionPerformed(evt);
            }
        });
        jPanel1.add(txtfuncion);
        txtfuncion.setBounds(60, 150, 170, 30);

        jButton1.setText("Calcular la Raiz");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(140, 410, 150, 30);

        jLabel3.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel3.setText("Valor para X");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(10, 200, 80, 15);

        txtx.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtxKeyTyped(evt);
            }
        });
        jPanel1.add(txtx);
        txtx.setBounds(90, 190, 100, 30);

        jLabel4.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel4.setText("Error Permitido");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 230, 130, 15);

        txterror.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txterrorKeyTyped(evt);
            }
        });
        jPanel1.add(txterror);
        txterror.setBounds(110, 230, 100, 30);

        jLabel5.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel5.setText("Numero de Iteraciones Max");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 270, 180, 30);

        txtni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtniKeyTyped(evt);
            }
        });
        jPanel1.add(txtni);
        txtni.setBounds(170, 270, 90, 30);

        jLabel6.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel6.setText("Raiz");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(10, 320, 40, 20);

        txtraiz.setEnabled(false);
        txtraiz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtraizActionPerformed(evt);
            }
        });
        jPanel1.add(txtraiz);
        txtraiz.setBounds(50, 320, 220, 30);

        jLabel7.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel7.setText("Funcion");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(10, 150, 60, 24);

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/log.png"))); // NOI18N
        jPanel1.add(jLabel10);
        jLabel10.setBounds(10, 10, 240, 120);

        jButton2.setText("Limpiar Campos");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(0, 410, 130, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(10, 10, 300, 450);

        jLabel8.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel8.setText("Tabla de Procedimiento y Solucion");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(510, 10, 260, 30);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tecmini.png"))); // NOI18N
        getContentPane().add(jLabel9);
        jLabel9.setBounds(760, 0, 180, 70);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void txtfuncionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfuncionActionPerformed
    }//GEN-LAST:event_txtfuncionActionPerformed
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Derivada Der=new Derivada();
        String data[][]={};
        String col[]={"i","Xi","f(Xi)","f'(Xi)","f''(Xi)","Xi+1","f(Xi+1)","Error"};
        Model=new DefaultTableModel(data,col);
        tabla.setModel(Model);
        con = 0;
        String fun,derivada,derivada2;
	double error,xi;
	int Ni;
        try {
            if(txtfuncion.getText().length() == 0 || txtx.getText().length() == 0 || txterror.getText().length() == 0 || txtni.getText().length() == 0){
                JOptionPane.showMessageDialog(null, "ALguna de las cajas esta vacia");
                txtfuncion.requestFocus();
                return;
            }
            JEP j = new JEP();
            j.addStandardConstants();
            j.addStandardFunctions();
            fun = txtfuncion.getText();
            derivada =Der.Der(fun);
            derivada2=Der.Der(derivada);
            Funcion CFun =new Funcion(fun);
            j.parseExpression(txtx.getText());// pasamos a convertr la expresion ingresada en la casilla x1
            xi = j.getValue();
            j.parseExpression(txterror.getText());// pasamos a convertr la expresion ingresada en la casilla  de tolerancia
            error = j.getValue();
            j.parseExpression(txtni.getText());// pasamos a convertr la expresion ingresada en la casilla  de toleranci
            Ni =(int)j.getValue();
            int i=0;
            double xi1;
            double delta;
            Derivada(derivada);
            Derivada2(derivada2);    
            do{
                i++;
                xi1 = xi - ( f(fun,xi) / ( f1(xi) -  ( (f(fun,xi)*f2(xi)) / (2*f1(xi)))  )   );
		
		delta = Math.abs(xi1-xi);				             
                Model.insertRow(con, new Object[]{});
                Model.setValueAt(i, con, 0);
                Model.setValueAt(xi, con, 1);
                Model.setValueAt(f(fun,xi), con, 2);
                Model.setValueAt(f1(xi), con, 3);
                Model.setValueAt(f2(xi), con, 4);
                Model.setValueAt(xi1, con, 5);
                Model.setValueAt(f(fun,xi1), con, 6);
                Model.setValueAt(delta, con, 7);
                con++;
                xi = xi1;
            }while(i<Ni && delta>error);
            Graficacion = new MGrafica("Grafica Newton 2do Orden Función: " + txtfuncion.getText().toString());
            txtraiz.setText(Double.toString(xi));
            Graficacion.AgregaDatos(CFun, xi);
            Graficacion.Agrega(xi,Ni);
            Graficacion.MuestraGrafica("Grafica Newton 2do Orden Función: " + txtfuncion.getText().toString());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR EN LA LECTURA DE DATOS", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed
    public void limpiar(){
        txtfuncion.setText("");
        txtx.setText("");
        txterror.setText("");
        txtni.setText("");
        txtraiz.setText("");
        Model=new DefaultTableModel();
        tabla.setModel(Model);   
    }
    public void Funcion(String f){
	funcion.addStandardFunctions();
	funcion.addStandardConstants();
	funcion.addVariable("x", 0);
        funcion.parseExpression(f);
    }
    public void Derivada(String f1){
        Derivada.addStandardFunctions();
	Derivada.addStandardConstants();
	Derivada.addVariable("x", 0);
	Derivada.parseExpression(f1);
    }
    public void Derivada2(String f2){
        Derivada2.addStandardFunctions();
	Derivada2.addStandardConstants();
	Derivada2.addVariable("x", 0);
	Derivada2.parseExpression(f2);
    }
    private double f(String funcion, double x){
        JEP f = new JEP();
        f.addStandardConstants();
        f.addStandardFunctions();
        f.addVariable("x", x);
        f.parseExpression(funcion); 
        return f.getValue();
    }
    public double f1(double x){
        Derivada.addVariable("x",x);
        return Derivada.getValue();
    }
    public double f2(double x){
        Derivada2.addVariable("x",x);
        return Derivada2.getValue();
    }
    private void txtDerivadaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDerivadaActionPerformed
    }//GEN-LAST:event_txtDerivadaActionPerformed
    private void txtraizActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtraizActionPerformed
    }//GEN-LAST:event_txtraizActionPerformed
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        limpiar();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txtxKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtxKeyTyped
        char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txtxKeyTyped

    private void txterrorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txterrorKeyTyped
       char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txterrorKeyTyped

    private void txtniKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtniKeyTyped
       char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txtniKeyTyped

    private void tablaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaKeyTyped
        char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_tablaKeyTyped

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txtDerivada;
    private javax.swing.JTextField txterror;
    private javax.swing.JTextField txtfuncion;
    private javax.swing.JTextField txtni;
    private javax.swing.JTextField txtraiz;
    private javax.swing.JTextField txtx;
    // End of variables declaration//GEN-END:variables
}
