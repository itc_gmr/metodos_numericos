import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.math.plot.Plot2DPanel;
import org.nfunk.jep.JEP;
public class MNewtonR extends javax.swing.JInternalFrame {
    Plot2DPanel grafica = new Plot2DPanel();
    JEP funcion = new JEP();
    MGrafica Graficacion;
    private DefaultTableModel Model;
    int con=0;
    public MNewtonR() {
        initComponents();
        setSize(960,500); 
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtfuncion = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txta = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txterror = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtni = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtraiz = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setTitle("Metodo de Newton Rapshon");
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel1.setLayout(null);

        txtfuncion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtfuncionActionPerformed(evt);
            }
        });
        jPanel1.add(txtfuncion);
        txtfuncion.setBounds(60, 140, 170, 30);

        jButton1.setText("Calcular la Raiz");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(130, 410, 130, 30);

        jLabel2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel2.setText("Intervalo: a");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(10, 190, 90, 15);

        txta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtaKeyTyped(evt);
            }
        });
        jPanel1.add(txta);
        txta.setBounds(80, 180, 80, 30);

        jLabel4.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel4.setText("Error Permitido");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 260, 130, 15);

        txterror.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txterrorKeyTyped(evt);
            }
        });
        jPanel1.add(txterror);
        txterror.setBounds(110, 260, 80, 30);

        jLabel5.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel5.setText("Numero de Iteraciones Max");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 220, 180, 30);

        txtni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtniKeyTyped(evt);
            }
        });
        jPanel1.add(txtni);
        txtni.setBounds(180, 220, 80, 30);

        jLabel6.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel6.setText("Raiz");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(10, 300, 40, 20);

        txtraiz.setEnabled(false);
        jPanel1.add(txtraiz);
        txtraiz.setBounds(40, 300, 220, 30);

        jLabel7.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel7.setText("Funcion");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(10, 140, 60, 24);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/log.png"))); // NOI18N
        jPanel1.add(jLabel3);
        jLabel3.setBounds(10, 10, 240, 120);

        jButton2.setText("Limpiar Campos");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(10, 410, 110, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(20, 10, 270, 450);

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabla.setEnabled(false);
        tabla.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tablaKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(tabla);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(300, 70, 630, 390);

        jLabel8.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        jLabel8.setText("Tabla de Procedimiento y Solucion");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(510, 10, 260, 30);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tecmini.png"))); // NOI18N
        getContentPane().add(jLabel9);
        jLabel9.setBounds(760, 0, 180, 70);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void txtfuncionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtfuncionActionPerformed
    }//GEN-LAST:event_txtfuncionActionPerformed
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Derivada Der=new Derivada();
        String data[][]={};
        String col[]={"i","Xi","f(Xi)","f'(Xi)","Xi+1","Error"};
        Model=new DefaultTableModel(data,col);
        tabla.setModel(Model);
        con = 0;
        try {
            if(txtfuncion.getText().length() == 0 || txta.getText().length() == 0 || txterror.getText().length() == 0 || txtni.getText().length() == 0){
                JOptionPane.showMessageDialog(null, "ALguna de las cajas esta vacia");
                txtfuncion.requestFocus();
                return;
            }
            JEP j = new JEP();
            j.addStandardConstants();
            j.addStandardFunctions();
            String fun = txtfuncion.getText();
            Funcion CFun =new Funcion(fun);
            String der =Der.Der(fun);
            j.parseExpression(txta.getText());// pasamos a convertr la expresion ingresada en la casilla x0
            double x = j.getValue();
            j.parseExpression(txterror.getText());// pasamos a convertr la expresion ingresada en la casilla  de tolerancia
            double e = j.getValue();
            int ni = Integer.parseInt(txtni.getText());// pasamos a convertr la expresion ingresada en la casilla  de numero maximo de iteraciones
            double s, delta;
            int i=1;
            int cont=0;
            double eps;
            do{
                s = x - (f(fun,x)/f(der,x));     
                delta = Math.abs(x-s);
                Model.insertRow(con, new Object[]{});
                Model.setValueAt(i, con, 0);
                Model.setValueAt(x, con, 1);
                Model.setValueAt(f(fun,x), con, 2);
                Model.setValueAt(f(der,x), con, 3);
                Model.setValueAt(s, con, 4);
                Model.setValueAt(delta, con, 5);
                con++;
                x = s;
                i++;
            }while(i<=ni && delta>e);
            Graficacion = new MGrafica("Grafica Newton Raphson Función: " + txtfuncion.getText().toString());
            txtraiz.setText(Double.toString(s));
            Graficacion.AgregaDatos(CFun,x);
            Graficacion.Agrega(s, ni);
            Graficacion.MuestraGrafica("Grafica Newton Raphson Función: " + txtfuncion.getText().toString());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR EN LA LECTURA DE DATOS", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        limpiar();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txtaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtaKeyTyped
        char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txtaKeyTyped

    private void txtniKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtniKeyTyped
      char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txtniKeyTyped

    private void txterrorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txterrorKeyTyped
       char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txterrorKeyTyped

    private void tablaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaKeyTyped
       char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_tablaKeyTyped
    private double f(String funcion, double x){
        JEP f = new JEP();
        f.addStandardConstants();
        f.addStandardFunctions();
        f.addVariable("x", x);
        f.parseExpression(funcion); 
        return f.getValue();
    }
    public void limpiar(){
        txtfuncion.setText("");
        txta.setText("");
        txterror.setText("");
        txtni.setText("");
        txtraiz.setText("");
        Model=new DefaultTableModel();
        tabla.setModel(Model);   
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txta;
    private javax.swing.JTextField txterror;
    private javax.swing.JTextField txtfuncion;
    private javax.swing.JTextField txtni;
    private javax.swing.JTextField txtraiz;
    // End of variables declaration//GEN-END:variables
}
