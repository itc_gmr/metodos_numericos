

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class SimpsonFuncion {

    ScriptEngineManager manager = new ScriptEngineManager();
    ScriptEngine engine = manager.getEngineByName("js");
    String funcion;
    double a;
    double b;
    double n;
    JTable table;
    double h;
    double x;
    double integral;
    int i = 1;

    public SimpsonFuncion(String funcion, double a, double b, double n, JTable table) {
        this.funcion = funcion;
        this.a = a;
        this.b = b;
        this.n = n;
        this.table = table;
    }

    public double solve() {
        String columnas[] = {"Iteraciones", "X", "Y", "FM", "FM*Y"};
        DefaultTableModel dtm = new DefaultTableModel(null, columnas);
        h = (b - a) / (n - 1);
        x = a;
        String masc = "##0.0####";
        int fm;
        float aux = 0;
        float aux1;
        double sum = 0.0;
        integral = (float) (f(a) + f(b));
       
        for (int k = 1; k < n + 1; k++) {

            if (k % 2 != 0) {
                fm = 4;
                aux = (float) (f(a + k * h) * fm + aux);
            }
            if (k % 2 == 0) {
                fm = 2;
                aux = (float) (f(a + k * h) * fm + aux);
            }
        }
        
        String row[] = {i + "", Formatea.alinder(masc, a) + "", Formatea.alinder(masc, f(a)) + "", Formatea.alinder(masc, 1) + "", Formatea.alinder(masc, f(a)) + ""};
        dtm.addRow(row);

        sum += Double.parseDouble(Formatea.alinder(masc, f(a)) + "");
        for (int k = 1; k < n - 1; k++) {
            i++;
            x = x + h;
            if (k % 2 != 0) {
                fm = 4;
                aux1 = (float) (f(a + k * h) * 4);
                
                sum += Double.parseDouble(Formatea.alinder(masc, aux1) + "");
                String rowif[] = {i + "", Formatea.alinder(masc, x) + "", Formatea.alinder(masc, f(x)) + "", Formatea.alinder(masc, fm) + "", Formatea.alinder(masc, aux1) + ""};
                dtm.addRow(rowif);
            }
            if (k % 2 == 0) {
                fm = 2;
                aux1 = (float) (f(a + k * h) * 2);
                
                sum += Double.parseDouble(Formatea.alinder(masc, aux1) + "");
                String rowif[] = {i + "", Formatea.alinder(masc, x) + "", Formatea.alinder(masc, f(x)) + "", Formatea.alinder(masc, fm) + "", Formatea.alinder(masc, aux1) + ""};
                dtm.addRow(rowif);
            }
           
        }
        
        sum += Double.parseDouble(Formatea.alinder(masc, f(b)) + "");
        String rowif[] = {n + "", Formatea.alinder(masc, b) + "", Formatea.alinder(masc, f(b)) + "", Formatea.alinder(masc, 1) + "", Formatea.alinder(masc, f(b)) + ""};
        dtm.addRow(rowif);
        integral = integral + aux;
        integral = (h / 3) * integral;
        integral = (h / 3) * sum;
        
        String fin [] = {"Total","","","",sum+""};
        dtm.addRow(fin);
        table.setModel(dtm);
        return integral;
    }

    public double f(double x) {
        engine.put("x", x);
        Object aux = null;
        try {
            aux = engine.eval(funcion);
        } catch (ScriptException e) {
           
            e.printStackTrace();
        }
        return Double.parseDouble(aux.toString());
    }
}
