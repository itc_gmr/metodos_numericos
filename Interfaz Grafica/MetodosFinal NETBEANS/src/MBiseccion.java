import java.awt.Color;
import java.awt.Panel;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.math.plot.Plot2DPanel;
import org.nfunk.jep.JEP;
public class MBiseccion extends javax.swing.JInternalFrame {
    Plot2DPanel grafica = new Plot2DPanel();
    JEP funcion = new JEP();
    MGrafica Graficacion;
    private DefaultTableModel Model;
    int con=0;
    frmMenuSolucionE OMenu=new frmMenuSolucionE();
    public MBiseccion() {
        initComponents();
        setSize(960,500); 
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtfuncion = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txta = new javax.swing.JTextField();
        txtb = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txterror = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtni = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtraiz = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();

        setTitle("Metodo de Biseccion\n");
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel1.setForeground(new java.awt.Color(67, 137, 137));
        jPanel1.setLayout(null);

        jLabel1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel1.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel1.setText("Funcion");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(10, 140, 60, 24);
        jPanel1.add(txtfuncion);
        txtfuncion.setBounds(70, 140, 170, 30);

        jButton1.setText("Calcular la Raiz");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton1MouseClicked(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(150, 410, 140, 30);

        jLabel2.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel2.setText("Intervalo: a");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(10, 190, 90, 15);

        jLabel3.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel3.setText("Intervalo: b");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(10, 230, 80, 15);

        txta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtaActionPerformed(evt);
            }
        });
        txta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtaKeyTyped(evt);
            }
        });
        jPanel1.add(txta);
        txta.setBounds(80, 180, 120, 30);

        txtb.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtbKeyTyped(evt);
            }
        });
        jPanel1.add(txtb);
        txtb.setBounds(80, 220, 120, 30);

        jLabel4.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel4.setText("Error Permitido");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(10, 270, 130, 15);

        txterror.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txterrorKeyTyped(evt);
            }
        });
        jPanel1.add(txterror);
        txterror.setBounds(110, 260, 130, 30);

        jLabel5.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel5.setText("Num. de Iteraciones Max");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(10, 290, 140, 50);

        txtni.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtniKeyTyped(evt);
            }
        });
        jPanel1.add(txtni);
        txtni.setBounds(160, 300, 120, 30);

        jLabel6.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel6.setText("Raiz");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(10, 350, 40, 20);

        txtraiz.setEnabled(false);
        txtraiz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtraizActionPerformed(evt);
            }
        });
        jPanel1.add(txtraiz);
        txtraiz.setBounds(40, 350, 220, 30);

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/log.png"))); // NOI18N
        jPanel1.add(jLabel7);
        jLabel7.setBounds(10, 10, 240, 120);

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/limpiar.png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);
        jButton2.setBounds(10, 410, 40, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(10, 10, 300, 450);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tecmini.png"))); // NOI18N
        getContentPane().add(jLabel9);
        jLabel9.setBounds(760, 0, 180, 70);

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabla.setEnabled(false);
        jScrollPane2.setViewportView(tabla);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(320, 80, 620, 370);

        jLabel8.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel8.setText("Tabla de Procedimiento y Solucion");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(390, 30, 350, 30);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Solucion();
    }//GEN-LAST:event_jButton1ActionPerformed
    public void Solucion(){
        con = 0;
        String data[][]={};
        String col[]={"i","a","b","Error","Xm","F(a)","F(xm)"};
        Model=new DefaultTableModel(data,col);
        tabla.setModel(Model);
        //try {
            if(txtfuncion.getText().length() == 0 || txta.getText().length() == 0 || txtb.getText().length() == 0 || txterror.getText().length() == 0 || txtni.getText().length() == 0){
                JOptionPane.showMessageDialog(null, "ALguna de las cajas esta vacia");
                txtfuncion.requestFocus();
                return;
            }
            JEP j = new JEP();
            j.addStandardConstants();
            j.addStandardFunctions();
            String fun = txtfuncion.getText();
            Funcion CFun =new Funcion(fun);
            j.parseExpression(txta.getText());
            double a = j.getValue();
            j.parseExpression(txtb.getText());
            double b = j.getValue();
            j.parseExpression(txterror.getText());
            double e = j.getValue();
            int ni = Integer.parseInt(txtni.getText());
            double p=a; 
            int i=1; 
            int cont=0;
            double eps;
            if((CFun.calc(a) * CFun.calc(b)) > 0){
		JOptionPane.showMessageDialog(null, "Puntos no válidos", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
                txta.requestFocus();
                txta.selectAll();
		return;
            }
            do{ 
                eps = Math.abs(b-a);
                p = (a+b)/2;
                Model.insertRow(con, new Object[]{});
                Model.setValueAt(i, con, 0);
                Model.setValueAt(a, con, 1);
                Model.setValueAt(b, con, 2);
                Model.setValueAt(eps, con, 3);
                Model.setValueAt(p, con, 4);
                Model.setValueAt(CFun.calc(a), con, 5);
                Model.setValueAt(CFun.calc(p), con, 6); 
                con++;
                if(CFun.calc(p)*CFun.calc(a)>0)
                    a=p;
                else 
                    if(CFun.calc(p)*CFun.calc(b)>0)
                        b=p;
                i++;
                cont++;
            }while(CFun.calc(p)!=0 & i<=ni & eps>e);
            Graficacion = new MGrafica("Metodo de Biseccion Función: " + txtfuncion.getText().toString());
            Graficacion.AgregaDatos(CFun,a);
            Graficacion.Agrega(p,ni);           
            txtraiz.setText(Double.toString(p));
            Graficacion.MuestraGrafica("Metodo de Biseccion Función: " + txtfuncion.getText().toString());           
        //}
        /*catch (Exception e) {
          //this.dispose();
          JOptionPane.showMessageDialog(null, "Error en los datos verifiquelos", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }*/
    }
    public void limpiar(){
        txtfuncion.setText("");
        txta.setText("");
        txtb.setText("");
        txterror.setText("");
        txtni.setText("");
        txtraiz.setText("");
        Model=new DefaultTableModel();
        tabla.setModel(Model);   
    }
    private void txtraizActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtraizActionPerformed
    }//GEN-LAST:event_txtraizActionPerformed
    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
    }//GEN-LAST:event_formKeyPressed
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        limpiar();
    }//GEN-LAST:event_jButton2ActionPerformed
    private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MouseClicked
 
    }//GEN-LAST:event_jButton1MouseClicked

    private void txtaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtaKeyTyped
      char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)
                                && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          } 
    }//GEN-LAST:event_txtaKeyTyped

    private void txtbKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbKeyTyped
     char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)
                                && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txtbKeyTyped

    private void txterrorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txterrorKeyTyped
       char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)
                                && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txterrorKeyTyped

    private void txtniKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtniKeyTyped
       char caracter=evt.getKeyChar(); 
        if(((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && (caracter !='.'))
            { 
            getToolkit().beep();   
            evt.consume(); 
            JOptionPane.showMessageDialog(null, "Ingrese solo numeros", "Mensaje de error", JOptionPane.ERROR_MESSAGE);
          }
    }//GEN-LAST:event_txtniKeyTyped

    private void txtaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tabla;
    private javax.swing.JTextField txta;
    private javax.swing.JTextField txtb;
    private javax.swing.JTextField txterror;
    private javax.swing.JTextField txtfuncion;
    private javax.swing.JTextField txtni;
    private javax.swing.JTextField txtraiz;
    // End of variables declaration//GEN-END:variables
}
