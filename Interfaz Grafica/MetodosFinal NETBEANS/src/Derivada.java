import org.lsmp.djep.djep.DJep;
import org.nfunk.jep.JEP;
import org.nfunk.jep.Node;
import org.nfunk.jep.ParseException;
public class Derivada{
	public  String Der(String fun) {                                         
        DJep j = new DJep();
        String derivada = "";
        j.addStandardConstants();
        j.addStandardFunctions();
        j.addComplex();
        j.setAllowUndeclared(true);
        j.setAllowAssignment(true);
        j.setImplicitMul(true);
        j.addStandardDiffRules();
        try{
            Node node = j.parse(fun);
            Node diff = j.differentiate(node,"x");
            Node simp = j.simplify(diff);
            derivada =j.toString(simp);
        } catch(ParseException e){ e.printStackTrace();}
		return derivada;
} 
	public  double f(String funcion, double x){
        JEP f = new JEP();
        f.addStandardConstants();
        f.addStandardFunctions();
        f.addVariable("x", x);
        f.parseExpression(funcion);
        return f.getValue();
    }
}