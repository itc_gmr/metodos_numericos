import javax.swing.*;
import org.jfree.chart.*;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.*;
public class MGrafica extends JFrame
{
	JFreeChart Grafica;
	JFreeChart Raz;
	XYSeriesCollection xy = new XYSeriesCollection();
	XYSeriesCollection RA = new XYSeriesCollection();
	XYSeries Datos = new XYSeries("Datos XY");
	XYSeries g = new XYSeries("G(X)");
	XYSeries R = new XYSeries("Raiz");
	int Clicks=1;
	ChartPanel Pan;
	ChartPanel Panel;
	public MGrafica(String nombre)
	{
		HazInterfaz();
	}
	public void HazInterfaz()
	{
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
	public void AgregaDatos(Funcion f , double a){
		for (double i = -(Math.abs(a)+15); i < Math.abs(a)+15; i++)
		{
			Datos.add(i, f.calc(i));
		}
	}
	
	public void Agregagx(Funcion f , double a){
		for (double i = -(Math.abs(a)+15); i < Math.abs(a)+15; i++)
		{
			g.add(i, f.calc(i));
		}
	}
	public void Agrega(double f , double a){
		for (double i = -(Math.abs(a)-5); i < Math.abs(a)-5; i++)
		{	
		R.add(i, f);
		}
	}
	public void MuestraGrafica(String nom){
		xy.addSeries(Datos);
		xy.addSeries(R);
		xy.addSeries(g);
		Grafica = ChartFactory.createXYLineChart(nom, "X", "F(X)", xy,PlotOrientation.VERTICAL, false, true, false);	
		Panel = new ChartPanel(Grafica);
		getContentPane().add(Panel);
		pack();
		setVisible(true);
	}
	public void GraficaVisible(){
		setVisible(true);
	}
}