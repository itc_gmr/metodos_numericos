package Componentes;

public class ListaDBL <T>{
	private NodoDBL<T> Frente;
	private NodoDBL<T> Fin;
	public  T Dr;
	public ListaDBL(){
		Frente=Fin=null;
		Dr=null;
	}
	public NodoDBL<T> DameFrente(){
		return Frente;
	}
	public NodoDBL<T> DameFin(){
		return Fin;
	}
	public boolean InsertaFre(T Dato){
		NodoDBL<T> Nuevo=new NodoDBL<T>(Dato);
		if(Nuevo==null)
			return false;
		// Primer nodo de la lista
		if(Frente==null){
			Frente=Fin=Nuevo;
			return true;
		}
		// Nuevo nodo al incio de la lisa
		Nuevo.setSiguiente(Frente);//Nuevo.Sig=Frente
		Frente.setAnterior(Nuevo);
		Frente=Nuevo;
		return true;
	}
	public boolean InsertaFin(T Dato){
		NodoDBL<T> Nuevo=new NodoDBL<T>(Dato);
		if(Nuevo==null)
			return false;
		// Primer nodo de la lista
		if(Frente==null){
			Frente=Fin=Nuevo;
			return true;
		}
		// Nuevo nodo al incio de la lisa
		Nuevo.setAnterior(Fin);
		Fin.setSiguiente(Nuevo);
		Fin=Nuevo;
		return true;
	}
	public boolean InsertaOrd(T Dato){
		NodoDBL<T> Nuevo=new NodoDBL(Dato);
		if(Nuevo==null)
			return false;
		if(Frente==null){
			Frente=Fin=Nuevo;
			return true;
		}
		NodoDBL<T> Ant=null,Aux=Frente;
		String IDNuevo=Dato.toString(); // IDNuevo=Nuevo.Info.toString()
		
		while (Aux != null && IDNuevo.compareTo(Aux.Info.toString()) >0  ){
			Ant=Aux;
			Aux=Aux.DameSig();
		}
		//Insertar el nuevo nodo al frente
		if(Ant==null){
			Frente.setAnterior(Nuevo);
			Nuevo.setSiguiente(Frente);
			Frente=Nuevo;
			return true;
		}
		// Insertar nuevo nodo al final de lallista
		if(Aux==null){
			Fin.setSiguiente(Nuevo);
			Nuevo.setAnterior(Fin);
			Fin=Nuevo;
			return true;
		}
		// entre dos nodos
		Nuevo.setSiguiente(Aux);
		Nuevo.setAnterior(Ant);
		Aux.setAnterior(Nuevo);
		Ant.setSiguiente(Nuevo);
		return true;
	}
	public boolean Busca(T Dato){
		String IdBusco=Dato.toString();
		NodoDBL<T> Aux=Frente;
		NodoDBL<T> Ant=null;
		boolean Band=false;
		
		while (Aux!=null ){
			if(IdBusco.compareTo(Aux.Info.toString())==0){
				Band=true;
				Dr=Aux.Info;
				break;
			}
			Ant=Aux;
			Aux=Aux.DameSig();
		}
		return Band;
	}
	public boolean Retira(T Dato){
		String IdBusco=Dato.toString();
		NodoDBL<T> Aux=Frente;
		NodoDBL<T> Ant=null;
		boolean Band=false;
		
		while (Aux!=null ){
			if(IdBusco.compareTo(Aux.Info.toString())==0){
				Band=true;
				Dr=Aux.Info;
				break;
			}
			Ant=Aux;
			Aux=Aux.DameSig();
		}
		if(!Band)
			return false;

		//unico nodo
		if(Frente==Fin){
			Frente=Fin=null;
			return true;
		}
		//Inicio
		if(Aux==Frente){
			Frente=Frente.DameSig();
			Frente.setAnterior(null);
			return true;
		}
		// final de la lista
		if(Aux==Fin){
			Fin=Ant;
			Fin.setSiguiente(null);
			return true;
		}
		Ant.setSiguiente(Aux.DameSig());
		Aux.DameSig().setAnterior(Ant);
		
		return true;
	}
}
