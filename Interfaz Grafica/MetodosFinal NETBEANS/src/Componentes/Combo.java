package Componentes;
import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.Vector;

import javax.swing.*;
public class Combo extends JPanel implements ActionListener{
	Vector vector;
	JComboBox combo;
	JButton ord, orig;
	Box colores;
	JLabel []col;
	String []V = {"Verde", "Azul", "Morado", "Rojo", "Anaranjado", "Azulote", "Rosilla", "Rojazo", "Verdeazul", "Azulverde"};
	String []Aux = new String[V.length];
	
	public Combo(){
		interfaz();
		escuchadores();
	}
	
	public void interfaz(){
		revuelve(V);
		for(int i = 0; i < Aux.length; i++){
			Aux[i] = V[i];
		}
		setVector();  //Convertir el arreglo en un objeto de tipo Vector
		combo = new JComboBox();
		combo.setEditable(true);
		combo.setModel(new DefaultComboBoxModel(vector));
		combo.setSelectedIndex(-1);
		JTextField text = (JTextField)combo.getEditor().getEditorComponent();
		text.setFocusable(true);
		text.setText("");
		text.addKeyListener(new ComboListener(combo,vector));
		
		col = new JLabel[V.length];
		colores = Box.createVerticalBox();
		ord = new JButton("Bot�n Ordenado");
		orig = new JButton("Bot�n Original");
		for(int i = 0; i < V.length; i++){
			col[i] = new JLabel(V[i]);
			colores.add(col[i]);
		}
		add(combo);
		add(colores);
		add(ord);
		add(orig);
		setVisible(true);
	}
	
	public void setVector(){
		vector = new Vector();
		for(int a=0;a<V.length;a++){
			vector.add(V[a]);
		}
	}
	
	public void escuchadores(){
		ord.addActionListener(this);
		orig.addActionListener(this);
		combo.addActionListener(this);
	}
	
	public void revuelve(String[] v2){
		System.out.println("Vector aleatorio");
		String auxx[] = new String[v2.length];
		Boolean auxB[] = new Boolean[v2.length];
		int nA;
		for(int i = 0; i < auxB.length; i++){
			auxB[i] = false;
		}
		for(int i = 0; i < auxx.length; i++){
			nA = MyRandom.nextInt(v2.length);
			if(auxB[nA]){
				i--;
				continue;
			}
			auxB[nA] = true;
			auxx[i] = v2[nA];
			System.out.println();
		}
		for(int i = 0; i < auxx.length; i++){
			v2[i] = auxx[i];
		}
	}
	
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource() == ord){
			Arrays.sort(V);
			colores.removeAll();
			for(int i = 0; i < V.length; i++){
				col[i] = new JLabel(V[i]);
				colores.add(col[i]);
			}
			combo = new JComboBox();
			setVector();
			combo = new JComboBox();
			combo.setEditable(true);
			combo.setModel(new DefaultComboBoxModel(vector));
			combo.setSelectedIndex(-1);
			JTextField text = (JTextField)combo.getEditor().getEditorComponent();
			text.setFocusable(true);
			text.setText("");
			text.addKeyListener(new ComboListener(combo,vector));
			add(combo, 0);
			remove(1);
			updateUI();
		}
		
		if(evt.getSource() == orig){
			colores.removeAll();
			for(int i = 0; i < V.length; i++){
				V[i] = Aux[i];
				col[i] = new JLabel(V[i]);
				colores.add(col[i]);
			}
			combo = new JComboBox();
			setVector();
			combo = new JComboBox();
			combo.setEditable(true);
			combo.setModel(new DefaultComboBoxModel(vector));
			combo.setSelectedIndex(-1);
			JTextField text = (JTextField)combo.getEditor().getEditorComponent();
			text.setFocusable(true);
			text.setText("");
			text.addKeyListener(new ComboListener(combo,vector));
			add(combo, 0);
			remove(1);
			updateUI();
		}
	}

}
