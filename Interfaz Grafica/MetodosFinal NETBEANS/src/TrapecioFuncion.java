

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class TrapecioFuncion {

    ScriptEngineManager manager = new ScriptEngineManager();
    ScriptEngine engine = manager.getEngineByName("js");
    String funcion;
    double a;
    double b;
    double n;
    JTable table;
    double h, x, y, integral;
    int i = 1;

    public TrapecioFuncion(String funcion, double a, double b, double n, JTable table) {
        this.funcion = funcion;
        this.a = a;
        this.b = b;
        this.n = n;
        this.table = table;
    }

    public double solve() {
        String columnas[] = {"Iteraciones", "X", "Y", "FM", "Resultado"};
        DefaultTableModel dtm = new DefaultTableModel(null, columnas);
        String masc = "##0.0####";
        h = (b - a) / (n - 1);
       
        x = a;
        int fm = 2;
        integral = (double) (f(a) + f(b));
        double aux = integral;
        double aux1;
        for (int k = 1; k < n - 1; k++) {
            aux = (double) (f(a + k * h) * fm + aux);
        }
       
        String[] filas = {i + "", Formatea.alinder(masc, a) + "", Formatea.alinder(masc, f(a)) + "", Formatea.alinder(masc, 1) + "", Formatea.alinder(masc, f(a)) + ""};
        dtm.addRow(filas);
        filas = null;
        for (int k = 1; k < n - 1; k++) {

            i++;
            x = x + h;
            aux1 = (double) (f(a + k * h) * fm);
           
            String rows[] = {i + "", Formatea.alinder(masc, x) + "", Formatea.alinder(masc, f(a + k * h)) + "", Formatea.alinder(masc, fm) + "", Formatea.alinder(masc, aux1) + ""};
            dtm.addRow(rows);
        }
       
        String row[] = {n + "", Formatea.alinder(masc, b) + "", Formatea.alinder(masc, f(b)) + "", Formatea.alinder(masc, 1) + "", Formatea.alinder(masc, f(b)) + ""};
        dtm.addRow(row);
        integral = (h / 2) * aux;
        
        double sum = 0.0;
        for (int i = 0; i < dtm.getRowCount(); i++) {
            sum += Double.parseDouble(dtm.getValueAt(i, dtm.getColumnCount() - 1) + "");
        }
        System.out.println(sum);
        String[] fila = {"Total:", "", "", "", Formatea.alinder(masc, sum) + ""};
        dtm.addRow(fila);
        table.setModel(dtm);
        return integral;
    }

    public double f(double x) {
        engine.put("x", x);
        Object aux = null;
        try {
            aux = engine.eval(funcion);
        } catch (ScriptException e) {
            
            e.printStackTrace();
        }
        return Double.parseDouble(aux.toString());
    }

}
