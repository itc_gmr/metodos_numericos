

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class TrapecioPuntos {
    
    JTable table;
    double h;
    int n;
    double integral;
    double y[];
    int i = 1;
    double sum = 0.0;
    
    public TrapecioPuntos(double[] y, JTable table, double h, int n) {
        this.table = table;
        this.y = y;
        this.h = h;
        this.n = n;
    }
    
    public double solve() {
        String columnas[] = {"Iteraciones", "Y", "FM", "Resultado"};
        DefaultTableModel dtm = new DefaultTableModel(null, columnas);
        String masc = "##0.0####";
        int fm = 2;
        System.out.println(y[0] + y[n]);///
        integral = y[0] + y[n];
        double aux = 0, aux1;
        for (int k = 1; k < n; k++) {
            aux = y[k] * fm + aux;
        }
      
        String row[] = {i + "", Formatea.alinder(masc, y[0]) + "", Formatea.alinder(masc, 1) + "", Formatea.alinder(masc, y[0]) + ""};
        dtm.addRow(row);
        sum += Double.parseDouble(Formatea.alinder(masc, y[0]) + "");
        for (int k = 1; k < n - 1; k++) {
            i++;
            aux1 = y[k] * fm;
           
            sum += Double.parseDouble(Formatea.alinder(masc, aux1) + "");
            String rows[] = {i + "", Formatea.alinder(masc, y[k]) + "", Formatea.alinder(masc, fm) + "", Formatea.alinder(masc, aux1) + ""};
            dtm.addRow(rows);
        }
      
        sum += Double.parseDouble(Formatea.alinder(masc, y[n - 1]) + "");
        String rowes[] = {n + "", Formatea.alinder(masc, y[n - 1]) + "", Formatea.alinder(masc, 1) + "", Formatea.alinder(masc, y[n - 1]) + ""};
        dtm.addRow(rowes);
        integral = integral + aux;
        integral = (h / 2) * integral;
        integral = (h / 2) * sum;
        
        String fin[] = {"Total:", "", "", sum + ""};
        dtm.addRow(fin);
        table.setModel(dtm);
        return integral;
    }
}
