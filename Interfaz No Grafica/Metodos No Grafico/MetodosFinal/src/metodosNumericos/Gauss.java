package metodosNumericos;

import java.util.Scanner;
public class Gauss {
	public static void Met() 
	{ 
		Scanner leer=new Scanner(System.in); 
		int n=0,m=0; 
		float matriz[][],t,suma,k[]; 
		System.out.print("Tama�o de la Matriz: "); 
		m=leer.nextInt(); 
		n=m+1;
		matriz=new float[m][n]; 
		k=new  float[m];
		for(int x=0;x<m;x++) 
		{ 
			for(int y=0;y<n;y++) 
			{ 
				System.out.print("Teclee el Valor para:  ");
				System.out.print("A["+(x)+"]["+(y)+"]: "); 
				matriz[x][y]=leer.nextFloat(); 
			} 
		}
		System.out.print("\n****************Matriz original****************"); 
		System.out.println(); 
		for(int x=0;x<m;x++) 
		{ 
			for(int y=0;y<n;y++) 
				System.out.print(matriz[x][y]+"\t"); 
			System.out.println(); 
		} 
		for(int x=0;x<m-1;x++) 
		{ 
			for(int y=x+1;y<m;y++) 
			{
				t=matriz[y][x]/matriz[x][x];
				for(int j=x;j<n;j++) 
				{
					matriz[y][j]=matriz[y][j]-t*matriz[x][j];
				}
			}
		}
		System.out.println("\n***************Matriz de Solucion***************"); 
		for(int x=0;x<m;x++) 
		{ 
			for(int y=0;y<n;y++) 
				System.out.print(" "+matriz[x][y]+"\t"); 
			System.out.println(); 
		} 
		k[m-1]=matriz[m-1][n-1]/matriz[m-1][m-1];
		for(int x=m-1;x>=0;x--) 
		{
			suma=0;
			for(int y=x+1;y<m;y++) 
			{
				suma=suma+matriz[x][y]*k[y];
			}
			k[x]=(matriz[x][n-1]-suma)/matriz[x][x];
		}
		System.out.println("***************Soluciones***************"); 
		for(int x=0;x<m;x++) 
		{ 
			System.out.println("solucion de X "+(x+1)+" es: "+k[x]); 
		}
	}
}