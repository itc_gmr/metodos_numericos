package metodosNumericos;

import java.io.*;

import org.nfunk.jep.JEP;
public class Leer
{
	static JEP j = new JEP();
	public static boolean error;
	public static String DString()
	{
		String Cad= "";
		try
		{
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader flujoE = new BufferedReader(isr);
			Cad = flujoE.readLine();
		}
		catch(IOException e)
		{
			System.err.println("Error: " + e.getMessage());
		}
    return Cad;
	}
	public static byte datoByte()
	{
		try
		{
			error=false;
			return Byte.parseByte(DString());
		}
		catch(NumberFormatException e)
		{   
			error=true;
			return Byte.MIN_VALUE;
		}
	}
	public static short datoShort()
	{
		try
		{
			return Short.parseShort(DString());
		}
		catch(NumberFormatException e)
		{
			return Short.MIN_VALUE;
		}
	} 
	public static int datoInt()
	{
		try
		{
			error=false;
			int val=Integer.parseInt(DString());
			if (val<-32768||val>32767)
    		error=true;
		return val;
		}
		catch(NumberFormatException e)
		{
			return Integer.MIN_VALUE;
		}
	}  
	public static long datoLong()
	{
		try
		{
			return Long.parseLong(DString());
		}
		catch(NumberFormatException e)
		{
			return Long.MIN_VALUE;
		}
	}
	public static float datoFloat()
	{
		try
		{
			error=false;
			Float f = new Float(DString());
			return f.floatValue();
		}
		catch(NumberFormatException e)
		{
			error=true;
			return Float.NaN;
		}
	}
	public static double datoDouble()
	{
		try
		{
			error=false;
			Double d = new Double(DString());
			return d.doubleValue();
		}
		catch(NumberFormatException e)
		{
			error=true;
			return Double.NaN;
		}
	}
	
	public static String funcion (String Funcion)
	{       String fx=Funcion;
		j.addStandardConstants();
		j.addStandardFunctions();
		j.setImplicitMul(true); 
		j.addVariable("x", 0);
		j.parseExpression(fx);
		
		if(j.hasError())
                    return "error"; //Hay error.
                
                j.parseExpression("");
		return fx;
	}

	public static double eval (String funcion, double x) 
	{
		j.parseExpression(funcion);
		j.addVariable("x", x);
		if(j.hasError())
                    j.getErrorInfo();
		return j.getValue();
	}

	public static double [] eval (String funcion, double [] x)
	{
		int n = x.length;
		double [] r = new double [n];
		
		for(int i=0; i<n; i++)
			r[i] = eval(funcion, x[i]);
		
		return r;
	}
}
