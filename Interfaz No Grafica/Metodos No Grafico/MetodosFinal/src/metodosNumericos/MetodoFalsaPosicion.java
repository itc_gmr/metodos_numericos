package metodosNumericos;

import org.nfunk.jep.*;
public class MetodoFalsaPosicion {
	JEP funcion = new JEP();
	MGrafica Grafica;
	public MetodoFalsaPosicion(){
		FalsaPos();
	}
	public void FalsaPos(){
		String fun;
		double a, b, e;
		int ni;
		Grafica = new MGrafica("Grafica Falsa Posici�n");
		System.out.println();
		System.out.println("Ingrese Valores");
		System.out.println();
		System.out.print("Funci�n: ");
		fun=Leer.DString();
		Funcion(fun);
		System.out.println("Intersecci�n: ");
		do{
			System.out.print("a= ");
			a=Leer.datoDouble();
			System.out.print("b= ");
			b=Leer.datoDouble();
			if(f(a)*f(b)>0){
				System.out.println("Ingrese un intervalo valido");
			}
		}while(f(a)*f(b)>0);
		System.out.print("Error permitido: ");
		e=Leer.datoDouble();
		System.out.print("N�mero de Iteraciones m�ximas: ");
		ni=Leer.datoInt();
		System.out.println();
		System.out.println();
		 double x1=a, xi=b, xim1=0,eps; 
         int i=1; 
         System.out.println(Pon.Blancos("i",5)+Pon.Blancos("Xi",20)+Pon.Blancos("f(Xi)",25)+Pon.Blancos("Xi-X1",20)+Pon.Blancos("f(Xi)-f(X1)",20)+Pon.Blancos("Xi+1",20)+Pon.Blancos("Error",25)+"f(Xi+1)");
         System.out.println(Pon.Blancos(i,5)+Pon.Blancos(x1,20)+Pon.Blancos(f(x1),25)+Pon.Blancos(x1-x1,20));
         Funcion CFun=new Funcion(fun);
         do{ 
			i++;
			xim1=xi-((f(xi)*(xi-x1))/(f(xi)-f(x1)));
			eps= Math.abs(xim1-xi);
			System.out.println(Pon.Blancos(i,5)+Pon.Blancos(xi,20)+Pon.Blancos(f(xi),25)+Pon.Blancos((xi-x1),20)+Pon.Blancos((f(xi)-f(x1)), 20)+Pon.Blancos(xim1,20)+Pon.Blancos(eps, 25)+f(xim1));
			xi = xim1;
         }while(i<=ni && eps>e);
		System.out.println("\nSoluci�n= " + xim1);
		Grafica.AgregaDatos(CFun,xi);
		Grafica.Agrega(xim1,ni);
		Grafica.MuestraGrafica("Grafica Falsa Posici�n Funci�n: " + fun);
	}
	public void Funcion(String f){
		funcion.addStandardFunctions();
		funcion.addStandardConstants();
		funcion.addVariable("x", 0);
		funcion.parseExpression(f);
	}
	public double f(double x){
		 funcion.addVariable("x",x);
		 return funcion.getValue();
	}
}
