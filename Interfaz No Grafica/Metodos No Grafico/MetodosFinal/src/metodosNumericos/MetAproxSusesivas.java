package metodosNumericos;

import org.nfunk.jep.*;
public class MetAproxSusesivas {
	MGrafica Grafica;
	public MetAproxSusesivas(){
		Aproximacion();
	}
	public void Aproximacion(){	
		String fun,fun2;
		double xi, e;
		int ni;
		Grafica = new MGrafica("Metodo de Aproximaciones Sucesivas");
		System.out.println("Teclee los valores de: ");
		System.out.print("Funci�n: ");
		fun=Leer.DString();
		System.out.print("g(x): ");
		fun2=Leer.DString();
		System.out.print("Xi= ");
		xi=Leer.datoDouble();
		System.out.print("Error permitido: ");
		e=Leer.datoDouble();
		System.out.print("N�mero de Iteraciones m�ximas: ");
		ni=Leer.datoInt();
		double ex;
        int i=0;
        System.out.println(Pon.Blancos("i",5)+Pon.Blancos("Xi",25)+Pon.Blancos("g(Xi)",25)+Pon.Blancos("Error",25)+"f(Xi)");
        do{
        	ex=f(fun2,xi)-xi;
        	i++;
        	System.out.println(Pon.Blancos(i,5)+Pon.Blancos(xi,25)+Pon.Blancos(f(fun2,xi),25)+Pon.Blancos(ex,25)+f(fun,xi));
        	xi=f(fun2,xi);
            ex=Math.abs(ex);
        }
        while(i<ni & ex>e);
        Funcion Cfun=new Funcion(fun);
        Funcion gx=new Funcion(fun2);
        Grafica.AgregaDatos(Cfun,xi );
        Grafica.Agregagx(gx,xi );
        System.out.println("\nSoluci�n= " + xi);
        Grafica.Agrega(xi,ni);
        Grafica.MuestraGrafica("Grafica Aproximaciones Sucesivas Funcion: " + fun);
    }
	private double f(String funcion, double x){
        JEP f = new JEP();
        f.addStandardConstants();
        f.addStandardFunctions();
        f.addVariable("x", x);
        f.parseExpression(funcion); 
        return f.getValue();
    }
}
