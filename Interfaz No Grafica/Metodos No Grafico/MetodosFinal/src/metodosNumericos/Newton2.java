package metodosNumericos;

import org.nfunk.jep.JEP;
public class Newton2 {
	MGrafica Grafica;
	JEP funcion = new JEP();
	JEP Derivada = new JEP();
	JEP Derivada2 = new JEP();
	public Newton2(){
		S2doOrden();
	}
	public void S2doOrden()
	{
		String fun,derivada,derivada2;
		double error,xi;
		int Ni;
		Derivada Der=new Derivada();
		Grafica = new MGrafica("Grafica Newton 2do Orden");
		System.out.println();
		System.out.println("Ingrese Valores");
		System.out.println();
		System.out.print("Funci�n: ");
		fun=Leer.DString();
		derivada=Der.Der(fun);
		derivada2=Der.Der(derivada);
		System.out.print("x: ");
		xi=Leer.datoDouble();
		System.out.print("Error permitido: ");
		error=Leer.datoDouble();
		System.out.print("N�mero de Iteraciones m�ximas: ");
		Ni=Leer.datoInt();
        int i=0;
        double xi1;
        double delta;
        Derivada(derivada);
		Derivada2(derivada2);
		System.out.println(Pon.Blancos("i",5)+Pon.Blancos("Xi",25)+Pon.Blancos("f(Xi)",25)+Pon.Blancos("f'(Xi)",25)+Pon.Blancos("f''(Xi)",20)+Pon.Blancos("Xi+1",20)+Pon.Blancos("f(Xi+1)",25)+"Error");
		do{
			i++;
			xi1 = xi - ( f(fun,xi) / ( f1(xi) -  ( (f(fun,xi)*f2(xi)) / (2*f1(xi)))  )   );
			delta = Math.abs(xi1-xi);
			//
			System.out.println(Pon.Blancos(i,5)+Pon.Blancos(xi,25)+Pon.Blancos(f(fun,xi),25)+Pon.Blancos(f1(xi),25)+Pon.Blancos(f2(xi),20)+Pon.Blancos(xi1,20)+Pon.Blancos(f(fun,xi1),25)+delta);
			xi=xi1;
		}while(i<Ni && delta>error);
		System.out.println("\nSoluci�n= " + xi);
		Funcion F=new Funcion(fun);
		Grafica.AgregaDatos(F, Ni);
		Grafica.Agrega(xi,Ni );
		Grafica.MuestraGrafica("Grafica Newton 2do Orden Funcion: " + fun);
	}
	public void Funcion(String f)
	{
		funcion.addStandardFunctions();
		funcion.addStandardConstants();
		funcion.addVariable("x", 0);
		funcion.parseExpression(f);
	}
	public void Derivada(String f1)
	{
		Derivada.addStandardFunctions();
		Derivada.addStandardConstants();
		Derivada.addVariable("x", 0);
		Derivada.parseExpression(f1);
	}
	public void Derivada2(String f2)
	{
		Derivada2.addStandardFunctions();
		Derivada2.addStandardConstants();
		Derivada2.addVariable("x", 0);
		Derivada2.parseExpression(f2);
	}
	private double f(String funcion, double x){
        JEP f = new JEP();
        f.addStandardConstants();
        f.addStandardFunctions();
        f.addVariable("x", x);
        f.parseExpression(funcion); 
        return f.getValue();
    }
	public double f1(double x)
	{
		Derivada.addVariable("x",x);
		 return Derivada.getValue();
	}
	public double f2(double x)
	{
		Derivada2.addVariable("x",x);
		 return Derivada2.getValue();
	}
}
